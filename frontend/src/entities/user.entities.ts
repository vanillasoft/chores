export interface User {
  username: string
  password: string
  accessToken?: string
  email?: string
}
