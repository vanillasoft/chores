import { defineStore } from 'pinia'
import UserService from '@/services/user.service'
import type { Chore } from '@/entities/chore.entities'

export const useChoreStore = defineStore('chore', {
  state: () => ({
    chores: [] as Array<Chore>,
  }),
  actions: {
    async getChores() {
      try {
        const result = await UserService.getUserChores()
        this.chores = result
      } catch (e) {
        this.chores = []
        throw new Error()
      }
    },
  },
})
