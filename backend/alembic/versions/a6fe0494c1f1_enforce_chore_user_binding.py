"""Bind Chores to user

Revision ID: a6fe0494c1f1
Revises: a6fe0494c1f0
Create Date: 2020-07-29 15:37:16.561484

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "a6fe0494c1f1"
down_revision = "a6fe0494c1f0"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute("UPDATE chore SET USER_ID = 1")

    with op.batch_alter_table("chore", schema=None) as batch_op:
        batch_op.alter_column("user_id", nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("chore", "user_id", nullable=True)
    # ### end Alembic commands ###
