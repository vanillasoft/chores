import axios from 'axios'

import type { Chore } from '@/entities/chore.entities'

function buildChore(schema: any): Chore {
  return {
    id: schema.id,
    name: schema.name,
    period: schema.period,
    lastExecutionDate: new Date(Date.parse(schema.last_execution_date)),
    description: schema.description,
    isOverdue: schema.is_overdue,
    dueDate: new Date(Date.parse(schema.next_execution_date)),
  }
}

export default {
  async getUserChores(): Promise<Chore[]> {
    const result: any = await axios.get('/chore')
    console.log(result)
    return result.chores.map(buildChore)
  },

  async getUserChore(choreId: number) {
    return axios.get(`/chore/${choreId}`)
  },

  async postUserChore(payload: any) {
    return axios.post('/chore', payload)
  },

  async putUserChore(choreId: number, payload: any) {
    return axios.put(`/chore/${choreId}`, payload)
  },

  async deleteUserChore(choreId: number) {
    return axios.delete(`/chore/${choreId}`)
  },

  async addUserTask(choreId: number) {
    return axios.post(`/chore/${choreId}`, {})
  },

  async deleteUserTask(taskId: number) {
    return axios.delete(`/task/${taskId}`)
  },
}
