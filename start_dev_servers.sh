#!/bin/bash

cd backend && FLASK_ENV="development" FLASK_APP="chores:create_app('development')" pipenv run flask run &
cd frontend && yarn serve &
echo $(jobs -p)
trap 'kill $(jobs -p)' EXIT
wait