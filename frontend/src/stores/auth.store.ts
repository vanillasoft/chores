import { defineStore } from 'pinia'
import type { User } from '@/entities/user.entities'
import AuthService from '@/services/auth.service'

const initialUser = JSON.parse(localStorage.getItem('local_user') || 'false')

export const useAuthStore = defineStore('auth', {
  state: () => ({
    status: { loggedIn: Boolean(initialUser) },
    user: initialUser,
  }),
  actions: {
    async login(credential: User) {
      try {
        const result = await AuthService.login(credential)
        localStorage.setItem('local_user', JSON.stringify(result))
        this.status.loggedIn = true
        this.user = result
      } catch (e) {
        this.status.loggedIn = false
        this.user = null
        throw new Error()
      }
    },
    async register(credential: User) {
      try {
        const result = await AuthService.register(credential)
        localStorage.setItem('local_user', JSON.stringify(result))
        this.status.loggedIn = true
        this.user = result
      } catch (e) {
        this.status.loggedIn = false
        this.user = null
        throw new Error()
      }
    },
    async logout() {
      this.status.loggedIn = false
      this.user = 'false'
      localStorage.removeItem('local_user')
    },
  },
  getters: {
    logged(): boolean {
      return this.status.loggedIn
    },
    getUsername(): string {
      return this.user.username
    },
  },
})
