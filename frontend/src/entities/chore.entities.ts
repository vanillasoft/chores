export interface Chore {
  id: number
  name: string
  period: number
  lastExecutionDate: Date
  description: string
  isOverdue: boolean
  dueDate: Date
}
