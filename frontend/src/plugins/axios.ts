import axios from 'axios'
import type { AxiosResponse } from 'axios'

axios.defaults.baseURL = import.meta.env.VITE_APP_API_URL
const cancelSource = axios.CancelToken.source()

axios.interceptors.request.use((config) => {
  config.cancelToken = cancelSource.token
  // If token are in local storage, add Bearer header to all request //
  const user = JSON.parse(localStorage.getItem('local_user')!)
  console.log(user)
  console.log(config.headers)
  if (user && user.access_token && config && config.headers) {
    config.headers['Authorization'] = `Bearer ${user.access_token}`
  }
  return config
})

const successMiddleware = (response: AxiosResponse) => {
  return response.data
}

axios.interceptors.response.use((response) => successMiddleware(response))
