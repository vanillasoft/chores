import axios from 'axios'

import type { User } from '@/entities/user.entities'

export default {
  async login(user: User): Promise<User> {
    return await axios.post('/auth/signin', {
      username: user.username,
      password: user.password,
    })
  },
  async register(user: User): Promise<User> {
    return await axios.post('/auth/signup', {
      username: user.username,
      password: user.password,
      email: user.email,
    })
  },
}
